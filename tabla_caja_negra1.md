|Descripción de las entradas|Nombre del test |
|------------|------------|
|Pasamos elemento que no es de tipo lista | A1 |
|Pasamos lista con menos de 2 enteros|A2|
| Pasamos 2 numeros y uno de tipo decimal | B|
|Pasamos 2 numeros en orden ascendente| C |
| Pasamos 2 numeros en orden descendente | D |
|Pasamos 3 numeros en orden ascendente| E |
| Pasamos 3 numeros en orden descendente | F |
|Pasamos 3 numeros con el 1º el mayor, el 2º el menor| G |
| Pasamos 3 números con el 1º el menor y el 2º el mayor | H |
| Pasamos 3 números 2º el mayor y 3º el menor | I |
| Pasamos 3 números 2º el menor y 3º el mayor | J |
| Pasamos 5 números y el 3,4 o 5 que no sea de tipo entero | k
