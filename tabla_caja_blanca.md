| Alternativa | Propiedad de las entradas | Nombre del test |
|-------------|---------------------------|-----------------|
|#1 True | No se introduce ningún argumento | A1 |
|#1 True | El tipo de a no sea una lista | A2 |
|#1 True | La longuitud de a sea menor que 1 | A3 |
|#1 False | A es una lista con minimo 1 numero | B |
| #2 True | A solo contiene 1 número | C |
|  #2 False | A contiene más de 1 numero | D |
|  #3 True | Segundo número es menor que el primero | E |
| #3 False | Segundo número es mayor que el primero | F |
| #4 0 veces | Longuitud de a menor que 2 | G |
| #4 1 vez | Longuitud de a igual que 2 | H |
| #4 Más de una vez | Longuitud de a mayor que 2 | I |
| #5 True | El número actual es menor que el minimo actual | J |
| #5 False | El número actual es mayor que el minimo actual| K |
| #6 True | El número actual es menor que el segundo minimo actual | L |
| #6 False | El número actual es mayor que el segundo minimo actual | M |
