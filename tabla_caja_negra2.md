|Descripción de las entradas|Nombre del test |
|------------|------------|
|No pasamos argumento | A |
|Pasamos día mayor de 31 |B|
| Pasamos dia 31 en mes que no hay 31 dias| C|
|Pasamos día negativo| D |
| Pasamos día 0 | E |
|Pasamos mes mayor que 12| F |
| Pasamos mes negativo | G |
|Pasamos mes 0| H |
| Pasamos año negativo | I |
| Pasamos 1 argumento| J |
| Pasamos 2 argumentos| K |
| Pasamos cualquier dia de cualquier mes y de cualquier año| L |
| Pasamos algún argumento diferente a entero | M |
