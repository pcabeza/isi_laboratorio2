from dos_menores import dos_menores

def test_A1():
	assert dos_menores() == None
	
def test_A2():
	assert dos_menores('a') == None
	
def test_A3():
	assert dos_menores([]) == None
	
def test_B():
	assert dos_menores([4,8,9, 3]) == (3, 4)
	
def test_C():
	assert dos_menores([3]) == (3)
	
def test_D():
	assert dos_menores([5,2,9,4]) == (2,4)

def test_E():
	assert dos_menores([8, 2]) == (2, 8)
	
def test_F():
	assert dos_menores([2,8]) == (2,8)
	
def test_G():
	assert dos_menores([3, 1]) == (1, 3)
	
def test_H():
	assert dos_menores([5,6]) == (5,6)
	
def test_I():
	assert dos_menores([8,5,7,3]) == (3, 5)
	
def test_J():
	assert dos_menores([5,7,2]) == (2,5)
	
def test_K():
	assert dos_menores([5,2,7]) == (2,5)
	
def test_L():
	assert dos_menores([2, 4, 3]) == (2,3)
	
def test_M():
	assert dos_menores([2, 4, 8]) == (2,4)
	
def test_total():
	assert dos_menores([2, 4, 8, -3, 109, 54, -2, 64]) == (-3,-2)
