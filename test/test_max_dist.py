from max_dist import max_dist, InvalidArgument
import pytest


@pytest.mark.parametrize("test_input,expected", [([4,7], 3), ([8,4],4), ([2,5,8],3), ([9, 5, 4], 4), ([9,-2,5], 11), 
	([2,45, 4], 43), ([12, 30, 1], 29), ([20, -3, 40], 43)])
	
def test_max_dist(test_input, expected):
    assert max_dist(test_input) == expected

def test_A1():
		with pytest.raises(InvalidArgument):
			assert max_dist("a"), "Function called with invalid arguments"

def test_A2():
		with pytest.raises(InvalidArgument):
			assert max_dist([8]), "Function called with invalid arguments"

def test_A2():
		with pytest.raises(TypeError):
			assert max_dist([8, 9.7])

def test_k():
	with pytest.raises(TypeError):
			assert max_dist([8, 9, 6, 5.8, 5])
