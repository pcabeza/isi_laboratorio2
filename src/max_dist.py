class InvalidArgument(Exception):
	"Function called with invalid arguments"
	pass

def max_dist (a = None):
	if type(a) != list or len(a) < 2:
		raise InvalidArgument()
		
	if type(a[0])!=int or type(a[1])!=int:
		raise TypeError
			
	num1 = a[0]
	num2 = a[1]
	max_dist = abs(num1-num2)
	
	for i in range(2, len(a)):
		
		if type(a[i])!=int:
			raise TypeError
			
		else:
			num1=a[i]
			aux_dist = abs(num1-num2)
			if aux_dist > max_dist:
				max_dist = aux_dist
			
			num2 = num1
			
	return max_dist
				
			
			
		
	
